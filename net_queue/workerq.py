import socket


class Worker(object):
    buffer = b''
    message_len = 0

    def __init__(self, **kwargs):
        self.host = kwargs["host"]
        self.port = kwargs["port"]
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect((self.host, self.port))

    def parse_pkg(self, chunk):
        self.buffer += chunk
        while True:
            if len(self.buffer) >= 10:
                self.message_len = int(self.buffer[0:10])
            if len(self.buffer) - 10 >= self.message_len:
                pkg = self.buffer[10:10 + self.message_len]
                self.buffer = self.buffer[10 + self.message_len:]
                self.message_len = 0
                yield pkg
            else:
                yield False

    def run(self, fn):
        while True:
            data = self.client.recv(1024)
            if data:
                for pkg in self.parse_pkg(data):
                    if pkg:
                        fn(pkg)
                    else:
                        break
            else:
                break
