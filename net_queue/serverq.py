import socket
from threading import Thread
from queue import Queue
import time


class Server(object):
    queue = Queue()
    CONST_LEN_BYTES = 10

    def __init__(self, **kwargs):
        self.host = kwargs["host"]
        self.port = kwargs["port"]
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind((self.host, self.port))
        self.server.listen()

    def pack(self, message):
        message = message.encode(encoding="utf-8")
        len_pkg = len(message)
        len_bytes = str(len_pkg)
        message_bytes = len_bytes + " " * (self.CONST_LEN_BYTES - len(len_bytes))
        return message_bytes.encode() + message

    def publisher(self, fn, timeout):
        while True:
            time.sleep(timeout)
            data = fn()
            for m in data:
                self.queue.put(self.pack(m))

    def getQueue(self, conn):
        while True:
            try:
                if not self.queue.empty():
                    conn.send(self.queue.get())
                else:
                    continue
            except (ConnectionResetError, BrokenPipeError):
                conn.close()

    def run(self, **kwargs):

        tp = Thread(target=self.publisher, args=(kwargs["add"], kwargs["timeout"],))
        tp.start()

        while True:
            connection, address = self.server.accept()
            if connection:
                t = Thread(target=self.getQueue, args=(connection,))
                t.start()

#     conn.send(self.queue.get())
# OSError: [Errno 9] Bad file descriptor