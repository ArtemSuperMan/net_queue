from net_queue.serverq import Server


def publish():
    messages = []
    for i in range(1000000):
        messages.append("message-{}".format(i))

    return messages


server = Server(host="127.0.0.1", port=15001)
server.run(add=publish, timeout=1)
