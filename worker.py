from net_queue.workerq import Worker


def handler(pkg):
    print(pkg.decode(encoding="utf-8"))


work = Worker(host="127.0.0.1", port=15001)
work.run(handler)
